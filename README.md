# **Blank Sage Document** 

This is a blank Jupyter notebook, used to load a SageMath 9.1 instance with the ore_algebra package installed. This instance can be used at [this link](https://mybinder.org/v2/git/https%3A%2F%2Fgit.uwaterloo.ca%2Fsmelczer%2Fblank-sage/HEAD?filepath=BlankSage.ipynb).
